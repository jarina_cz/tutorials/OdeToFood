# OdeToFood ASP.NET Core 2.1 App 

Application written during the Pluralsight's [ASP.NET Core Fundamentals](https://www.pluralsight.com/courses/aspdotnet-core-fundamentals) course by Scott Allen

## How to follow
* commit was created after every section of course with descriptive names, so it should be easy to follow what was done and why. Some hidden details (like dotnet cli uses) will be written in the section bellow

## Additional stuff done

### After Configuring the Entity Framework Services
* Github commit [b68b270](https://github.com/stefanjarina/OdeToFood/commit/b68b2703ea1b2678b76413154b1ea44568ec9eaf)

```bash
$ dotnet ef dbcontext list

OdeToFood.Data.OdeToFoodDbContext
```

```bash
dotnet ef dbcontext info

Provider name: Microsoft.EntityFrameworkCore.SqlServer
Database name: OdeToFood
Data source: (localdb)\MSSQLLocalDB
Options: None
```

```bash
$ dotnet ef migrations add InitialCreateMigration

info: Microsoft.EntityFrameworkCore.Infrastructure[10403]
      Entity Framework Core 2.0.1-rtm-125 initialized 'OdeToFoodDbContext' using provider 'Microsoft.EntityFrameworkCore.SqlServer' with options: None
Done. To undo this action, use 'ef migrations remove'
```

```bash
$ dotnet ef database update
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (681ms) [Parameters=[], CommandType='Text', CommandTimeout='60']
      CREATE DATABASE [OdeToFood];
... output ommitted for readability ...
info: Microsoft.EntityFrameworkCore.Database.Command[20101]
      Executed DbCommand (1ms) [Parameters=[], CommandType='Text', CommandTimeout='30']
      INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
      VALUES (N'20171202190159_InitialCreateMigration', N'2.0.1-rtm-125');
Done.
```

## Technologies learned during this course
* [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language))
* [.NET Core 2.1](https://www.microsoft.com/net)
* [ASP.NET Core 2.1](https://www.asp.net/)
* [ASP.NET MVC](https://www.asp.net/mvc)
* [Entity Framework Core 2.1](https://docs.microsoft.com/en-us/ef/core/)
* [Razor Pages](https://docs.microsoft.com/en-us/aspnet/core/mvc/razor-pages)
